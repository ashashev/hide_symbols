#ifndef __BETA_DECL_H__
#define __BETA_DECL_H__

#ifdef BUILDING_BETA
#define BETA_EXPORTED __attribute__((__visibility__("default")))
#else
#define BETA_EXPORTED
#endif

#endif //!__BETA_DECL_H__
