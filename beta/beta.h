#ifndef __BETA_H__
#define __BETA_H__

#include <string>
#include <beta_decl.h>

namespace beta
{

BETA_EXPORTED std::string info();

} /* namespace beta */

#endif //!__BETA_H__
