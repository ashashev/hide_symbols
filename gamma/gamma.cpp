#include <iostream>
#include <alpha.h>
#include <beta.h>

int main()
{
    std::cout << "Hi from gamma!" << "\n";
    std::cout << "Info from alpha: " << alpha::info() << "\n";
    std::cout << "Info from beta: " << beta::info() << "\n";
    return 0;
}
