#include "alpha.h"

std::string make_info(int major, int minor, int patch)
{
    std::string info = "alpha." + 
                       std::to_string(major) + "." +
                       std::to_string(minor) + "." +
                       std::to_string(patch);
    return info;
}

std::string alpha::info()
{
    return make_info(0, 1, 0);
}
