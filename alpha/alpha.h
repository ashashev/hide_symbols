#ifndef __ALPHA_H__
#define __ALPHA_H__

#include <string>
#include <alpha_decl.h>

namespace alpha
{

ALPHA_EXPORTED std::string info();

} /* namespace alpha */

#endif //!__ALPHA_H__
