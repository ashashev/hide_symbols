#ifndef __ALPHA_DECL_H__
#define __ALPHA_DECL_H__

#ifdef BUILDING_ALPHA
#define ALPHA_EXPORTED __attribute__((__visibility__("default")))
#else
#define ALPHA_EXPORTED
#endif

#endif //!__ALPHA_DECL_H__
